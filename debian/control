Source: tiledb-r
Section: gnu-r
Priority: optional
Maintainer: Dirk Eddelbuettel <edd@debian.org>
Build-Depends: debhelper-compat (= 12), r-base-dev (>= 4.2.2), dh-r, libtiledb-dev, r-cran-rcpp, r-cran-nanotime, r-cran-spdl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/edd/r-cran-tiledb
Vcs-Git: https://salsa.debian.org/edd/r-cran-tiledb.git
Homepage: https://cran.r-project.org/package=tiledb

Package: r-cran-tiledb
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${R:Depends}, r-cran-rcpp, r-cran-nanotime, r-cran-spdl
Suggests: r-cran-tinytest, r-cran-simplermarkdown, r-cran-curl, r-cran-bit64, r-cran-matrix, r-cran-palmerpenuins, r-cran-data.table, r-cran-tibble
Description: GNU R package for the TileDB Universal Storage Engine
 The universal storage engine 'TileDB' introduces a powerful on-disk
 format for multi-dimensional arrays. It supports dense and sparse
 arrays, dataframes and key-values stores, cloud storage ('S3', 'GCS',
 'Azure'), chunked arrays, multiple compression, encryption and
 checksum filters, uses a fully multi-threaded implementation,
 supports parallel I/O, data versioning ('time travel'), metadata and
 groups. It is implemented as an embeddable cross-platform C++ library
 with APIs from several languages, and integrations.
